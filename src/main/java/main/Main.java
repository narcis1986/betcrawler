package main;

import engine.CrawlerEngine;
import engine.IConnection;
import engine.PageConnection;

/**
 * Created by Narcis on 8/7/2016.
 * <p>
 * Starting point of the application
 */
public class Main {
    public static void main(String[] args) {
        IConnection connection = new PageConnection();
        CrawlerEngine app = new CrawlerEngine(connection);
        app.findEligibleMatches();
    }
}
