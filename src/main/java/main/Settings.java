package main;

import org.apache.log4j.Logger;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by Narcis on 8/7/2016.
 */
public class Settings {

    private static final Logger log = Logger.getLogger(Settings.class.getName());

    private String _nrMatchesForm;
    private String _url;

    private static Settings _instance;

    private Settings(){}

    public static Settings Get(){

        if(_instance == null){
            _instance = new Settings();
            _instance.loadSettings();
        }

        return _instance;
    }

    private void loadSettings() {
        Properties prop = new Properties();
        InputStream input = null;

        try {
            ClassLoader loader = Thread.currentThread().getContextClassLoader();
            input = loader.getResourceAsStream("config.properties");
            prop.load(input);

            // get the property value and print it out
            _nrMatchesForm = prop.getProperty("HistoryForm");
            _url = prop.getProperty("URL");
            log.info("Loaded property NrOfMatchesForm" + _nrMatchesForm);

        } catch (IOException ex) {
            log.error(ex);
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    log.error(e);
                }
            }
        }
    }

    public Integer HISTORY_FORM() {
        return Integer.parseInt(_nrMatchesForm);
    }

    public String URL(){
        return _url;
    }
}
