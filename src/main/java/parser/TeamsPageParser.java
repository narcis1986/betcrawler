package parser;

import com.gargoylesoftware.htmlunit.html.DomNode;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlTableCell;
import model.EForm;
import model.Game;
import model.Team;
import org.apache.log4j.Logger;

import java.util.List;

/**
 * Created by Narcis on 8/7/2016.
 */
public class TeamsPageParser implements IParser {

    private static Logger log = Logger.getLogger(MainPageParser.class.getName());

    private Team _host;
    private Team _guest;
    private HtmlPage _page;
    private List<Game> _matches;
    private List<?> _teamsRow;

    public TeamsPageParser(HtmlPage page) {
        _page = page;
    }

    @Override
    public void parse() {
        getTeamsName();
        getMatchStatistics();

    }

    public Game getMatch() {
        return new Game(_host, _guest);
    }

    private void getTeamStatistics(DomNode tr, Team team) {

        HtmlTableCell formCell = (HtmlTableCell) tr.getLastChild();
        if (formCell == null) return;

        for (DomNode frm : formCell.getFirstChild().getChildNodes()) {
            String clsAttr = frm.getAttributes().getNamedItem("class").getTextContent();
            if (clsAttr.contains("form-w")) {
                team.addTeamForm(EForm.WIN);
            } else if (clsAttr.contains("form-d")) {
                team.addTeamForm(EForm.DRAW);

            } else if (clsAttr.contains("form-l")) {
                team.addTeamForm(EForm.LOSE);
            }
        }
    }

    private void getMatchStatistics() {
        List<DomNode> spans = (List<DomNode>) _page.getByXPath("//span[@class='team_name_span']");

        for (DomNode span : spans) {
            if (span.getTextContent().equalsIgnoreCase(_host.getName())) {
                getTeamStatistics(span.getParentNode().getParentNode().getParentNode(), _host);
            } else if (span.getTextContent().equalsIgnoreCase(_guest.getName())) {
                getTeamStatistics(span.getParentNode().getParentNode().getParentNode(), _guest);
            }
        }
    }

    private void getTeamsName() {
        DomNode node = _page.getFirstByXPath("//h1[@class='nextmatch']");

        if (node != null) {
            String teamNames = node.getChildNodes().get(1).getFirstChild().getTextContent();

            String[] teams = teamNames.split(" - ");

            if(teams[0].isEmpty() || teams[1].isEmpty())
                throw new RuntimeException("cannot get the team names from url " + _page.getUrl());

            _host = new Team(teams[0]);
            _guest = new Team(teams[1]);

        }
    }
}
