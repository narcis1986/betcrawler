package parser;

import com.gargoylesoftware.htmlunit.html.HtmlAnchor;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlTableCell;
import model.Game;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Narcis on 8/7/2016.
 * This is the page where all the football matches are parsed.
 * Here we will extract  http://www.betexplorer.com/next/soccer/ all the matches that will be played today.
 * For every match we will check the team forms and then decide if the match is good for a bet
 */
public class MainPageParser implements IParser {


    private static Logger log = Logger.getLogger(MainPageParser.class.getName());

    private List<Game> _games = new LinkedList();
    private HtmlPage _page;

    public MainPageParser(HtmlPage page) {
        _page = page;
    }

    @Override
    public void parse() {
        List<HtmlTableCell> cells = (List<HtmlTableCell>) _page.getByXPath("//td[@class='tv']");

        log.info("Found " + cells.size() + " possible matches");
        cells.parallelStream().forEach(cell -> {
            try{
                HtmlAnchor anchor = getTeamsDetailsUrl(cell);
                HtmlPage teamsPage = clickAndWaitForJavascript(anchor);
                if (teamsPage != null) {
                    TeamsPageParser parser = new TeamsPageParser(teamsPage);
                    parser.parse();
                    _games.add(parser.getMatch());
                }
            }catch (Exception e){
                log.error(e);
            }

        });
    }

    public List<Game> getGames(){
        return _games;
    }

    private HtmlAnchor getTeamsDetailsUrl(HtmlTableCell cell) {
        try {
            return (HtmlAnchor) cell.getPreviousSibling().getFirstChild();
        }catch (Exception e){
            return null;
        }
    }

    private HtmlPage clickAndWaitForJavascript(HtmlAnchor anchor) {

        if (anchor == null) return null;

        HtmlPage page = null;
        try {
            page = anchor.click();
        } catch (IOException e) {
            log.error(e);
        }

        synchronized (page) {
            try {
                page.wait(2000);  // How often to check
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        return page;
    }
}
