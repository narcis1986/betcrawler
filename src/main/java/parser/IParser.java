package parser;

/**
 * Created by Narcis on 8/7/2016.
 */
public interface IParser {
    void parse();
}
