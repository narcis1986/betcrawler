package engine;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.SilentCssErrorHandler;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.logging.Level;

public class PageConnection implements IConnection {

    private static final Logger log = Logger.getLogger(PageConnection.class.getName());
    private static WebClient _webClient;

    private static WebClient GetWebClient() {
        if (_webClient != null)
            return _webClient;
        _webClient = new WebClient(BrowserVersion.FIREFOX_45);
        _webClient.waitForBackgroundJavaScript(1000);
        _webClient.getCookieManager().setCookiesEnabled(true);
        _webClient.getOptions().setJavaScriptEnabled(true);
        _webClient.getOptions().setTimeout(2000);
        _webClient.getOptions().setUseInsecureSSL(true);
        _webClient.getOptions().setThrowExceptionOnScriptError(false);
        _webClient.getOptions().setPrintContentOnFailingStatusCode(false);
        _webClient.setCssErrorHandler(new SilentCssErrorHandler());

        java.util.logging.Logger.getLogger("com.gargoylesoftware").setLevel(Level.OFF);
        LogFactory.getFactory().setAttribute("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.NoOpLog");
        java.util.logging.Logger.getLogger("com.gargoylesoftware.htmlunit").setLevel(Level.OFF);
        java.util.logging.Logger.getLogger("org.apache.commons.httpclient").setLevel(Level.OFF);

        return _webClient;
    }

    public HtmlPage getPage(String url) {
        try {
            final HtmlPage page = GetWebClient().getPage(url);
            synchronized (page) {
                try {
                    page.wait(2000);  // How often to check
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return page;
        } catch (FailingHttpStatusCodeException e) {
            log.error(e);
        } catch (MalformedURLException e) {
            log.error(e);
        } catch (IOException e) {
            log.error(e);
        }

        return null;
    }

}
