package engine;

import com.gargoylesoftware.htmlunit.html.HtmlPage;

/**
 * Created by Narcis on 8/7/2016.
 */
public interface IConnection {

    HtmlPage getPage(String url);

}
