package engine;

import com.gargoylesoftware.htmlunit.html.HtmlPage;
import main.Settings;
import model.Game;
import model.NotEnoughDataException;
import model.Team;
import org.apache.log4j.Logger;
import parser.MainPageParser;

import java.util.List;

public class CrawlerEngine {

    private static final Logger log = Logger.getLogger(CrawlerEngine.class.getName());

    IConnection _connection;

    public CrawlerEngine(IConnection con) {
        _connection = con;
    }

    /*
    Find all the matches where the host team won the last X games and the guest team lost the last X games
     */
    public void findEligibleMatches() {
        final HtmlPage page = _connection.getPage(Settings.Get().URL());

        if (page == null) {
            log.error("Cannot connect to URL " + Settings.Get().URL());
            return;
        }

        MainPageParser parser = new MainPageParser(page);
        parser.parse();
        List<Game> games = parser.getGames();
        games.stream().forEach(game -> {
            if(game.isGoodToBet()){
                Team teamToWin = null;
                try {
                    if (game.getHost().wonLastGames() && game.getGuest().didNotWinLastGames()){
                        teamToWin = game.getHost();
                    } else if(game.getHost().didNotWinLastGames() && game.getGuest().wonLastGames()){
                        teamToWin = game.getGuest();
                    }
                } catch (NotEnoughDataException e){
                    log.warn("Not enough data to evaluate the game " + game.getHost().getName() + " " + game.getGuest().getName());
                }

                if(teamToWin != null){
                    log.info("You should BET on Team " + teamToWin.getName());
                }

            }
        });
    }

}
