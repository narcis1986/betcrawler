package model;

/**
 * Created by Narcis on 8/8/2016.
 */
public class NotEnoughDataException extends Exception {

    public NotEnoughDataException(String message){
        super(message);
    }
}
