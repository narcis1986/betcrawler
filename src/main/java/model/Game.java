package model;

import org.apache.log4j.Logger;

/**
 * Created by Narcis on 8/7/2016.
 */
public class Game {
    private Team _host;
    private Team _guest;

    private static final Logger log = Logger.getLogger(Game.class.getName());

    public Game(Team host, Team guest) {
        _host = host;
        _guest = guest;
    }

    public Team getHost() {
        return _host;
    }

    public void setHost(Team host) {
        this._host = host;
    }

    public Team getGuest() {
        return _guest;
    }

    public void setGuest(Team guest) {
        this._guest = guest;
    }

    public boolean isGoodToBet() {
        try {
            if (getHost().hasPlayedEnoughGames()
                    && getGuest().hasPlayedEnoughGames()) {
                boolean hostWonLastGames = getHost().wonLastGames();
                boolean guestsWonLastGames = getGuest().wonLastGames();
                if ((hostWonLastGames && !guestsWonLastGames)
                        || guestsWonLastGames && !hostWonLastGames)
                    return true;
            }
        } catch (NotEnoughDataException e) {
            log.error(e);
        } catch (Exception e) {
            log.error(e);
        }


        return false;
    }

}
