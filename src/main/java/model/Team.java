package model;

import main.Settings;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Narcis on 8/7/2016.
 */
public class Team {
    private String _name;
    private List<EForm> _form = new ArrayList<>();

    private static final Logger log = Logger.getLogger(Team.class.getName());

    public Team(String name) {
        _name = name;
    }

    public String getName() {
        return _name;
    }

    public void setName(String name) {
        this._name = name;
    }

    public void addTeamForm(EForm form) {
        _form.add(form);
    }

    public List<EForm> getForm(){
        return _form;
    }

    public boolean hasPlayedEnoughGames(){
        return _form.size() >= Settings.Get().HISTORY_FORM();
    }

    public boolean wonLastGames() throws NotEnoughDataException{
        if(!hasPlayedEnoughGames())
            throw new NotEnoughDataException("There are not enough games played");

        for (int i = 0; i < Settings.Get().HISTORY_FORM(); i++){
            if(_form.get(i) != EForm.WIN)
                return false;
        }

        return true;
    }

    public boolean didNotWinLastGames() throws NotEnoughDataException{
        return !wonLastGames();
    }

}
